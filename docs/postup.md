# Postup

## Marinování masa

1. Hovězí maso omyjte a osušte.
2. V misce smíchejte olej, ocet, sůl, pepř, bobkové listy, nové koření a kuličky pepře.
3. Maso vložte do marinády a nechte v lednici marinovat alespoň 2 hodiny, ideálně přes noc.

## Příprava zeleniny

1. Mrkev, petržel a celer oloupejte a nakrájejte na kostičky.
2. Cibuli oloupejte a nakrájejte na jemno.

## Vaření masa

1. V hrnci rozehřejte máslo a přidejte nakrájenou zeleninu.
2. Zeleninu osmahněte dozlatova.
3. Přidejte marinované maso a ze všech stran ho opečte.
4. Přilijte vodu tak, aby bylo maso ponořené, a přiveďte k varu.
5. Přikryjte pokličkou a na mírném ohni vařte, dokud není maso měkké (asi 2 hodiny).

## Příprava omáčky

1. Když je maso měkké, vyjměte ho z hrnce a udržujte teplé.
2. Z hrnce odstraňte bobkové listy a nové koření.
3. Zeleninu rozmixujte dohladka.
4. Přidejte smetanu ke šlehání a mouku rozpuštěnou ve troše vody.
5. Omáčku dochuťte cukrem, citronovou šťávou, solí a pepřem.
6. Povařte, dokud omáčka nezhoustne.

## Servírování

1. Maso nakrájejte na plátky a podávejte s omáčkou.
2. Jako přílohu podávejte houskový knedlík.
