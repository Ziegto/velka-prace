import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

from funkce import soucet

def test_soucet():
    assert soucet(2, 3) == 5
    assert soucet(-1, 1) == 0
    assert soucet(0, 0) == 0

from funkce2 import faktorial, fibonacci, is_prime, sort_list, gcd, lcm

def test_faktorial():
    assert faktorial(0) == 1
    assert faktorial(1) == 1
    assert faktorial(5) == 120
    assert faktorial(10) == 3628800

def test_fibonacci():
    assert fibonacci(0) == 0
    assert fibonacci(1) == 1
    assert fibonacci(2) == 1
    assert fibonacci(10) == 55

def test_is_prime():
    assert not is_prime(0)
    assert not is_prime(1)
    assert is_prime(2)
    assert not is_prime(4)
    assert is_prime(13)
    assert not is_prime(15)

def test_sort_list():
    assert sort_list([]) == []
    assert sort_list([5, 1, 4, 2, 8]) == [1, 2, 4, 5, 8]
    assert sort_list([3, 3, 3, 3]) == [3, 3, 3, 3]
    assert sort_list([-1, -5, 0, 3, -2]) == [-5, -2, -1, 0, 3]

def test_gcd():
    assert gcd(8, 12) == 4
    assert gcd(17, 23) == 1
    assert gcd(14, 28) == 14

def test_lcm():
    assert lcm(8, 12) == 24
    assert lcm(17, 23) == 391
    assert lcm(14, 28) == 28
