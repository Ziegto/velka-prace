def faktorial(n):
    """
    Funkce pro výpočet faktoriálu čísla n.
    """
    if n == 0:
        return 1
    else:
        return n * faktorial(n - 1)


def fibonacci(n):
    """
    Funkce pro výpočet n-tého čísla ve Fibonacciho posloupnosti.
    """
    a, b = 0, 1
    for _ in range(n):
        a, b = b, a + b
    return a


def is_prime(n):
    """
    Funkce pro kontrolu, zda je číslo n prvočíslo.
    """
    if n <= 1:
        return False
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True


def sort_list(lst):
    """
    Funkce pro seřazení seznamu lst pomocí algoritmu bubble sort.
    """
    n = len(lst)
    for i in range(n):
        for j in range(0, n-i-1):
            if lst[j] > lst[j+1]:
                lst[j], lst[j+1] = lst[j+1], lst[j]
    return lst


def gcd(a, b):
    """
    Funkce pro výpočet největšího společného dělitele (GCD) dvou čísel.
    """
    while b:
        a, b = b, a % b
    return a


def lcm(a, b):
    """
    Funkce pro výpočet nejmenšího společného násobku (LCM) dvou čísel.
    """
    return abs(a*b) // gcd(a, b)
